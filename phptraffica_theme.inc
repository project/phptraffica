<?php
function theme_phptraffica_user_phrases($plist = array() ){
    if(count($plist) )foreach($plist as $key => $value) 
        $plist[$key] = '<em>' . check_plain(trim(str_replace(',', '', $value) ) ) . '</em>';
    $phrases = implode(', ', $plist);
    return t('People find this page by phrases:') . '<br />' . $phrases;
}
function theme_phptraffica_seo_phrases_block($plist = array() ){
    $header = array(t('Search Engine'), t('Keywords'), t('Count') );
    $rows = array();
    if(count($plist) ) foreach($plist as $v) $rows []= array($v['engine'], $v['keyword'], $v['count'] );
    return theme('table', $header, $rows);
}
function theme_phptraffica_seo_referers_block($plist = array() ){
    $header = array(t('URL'), t('Count') );
    $rows = array();
    if(count($plist) ) foreach($plist as $v) $rows []= array(
        array('data' => l(substr($v['address'], 0, 15), $v['address']), 'class' => 'phptraffica-count1' ), 
        array('data' => $v['count'], 'class' => 'phptraffica-count' ) 
    );
    return theme('table', $header, $rows);
}
